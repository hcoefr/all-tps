# II. Servicer le programme

**Première chose, plutôt que de lancer ce programme à la main, on va en faire un *service*.**  
Dans les OS GNU/Linux modernes, c'est *systemd* qui gère les services : il les lance, les relance s'ils crashent, les surveille, etc.

> *systemd* est un programme présent par défaut dans les OS GNU/Linux modernes. La commande `systemctl` permet d'ordonner des choses à *systemd* comme le démarrage d'un *service*.

Un *service* c'est simplement un programme que *systemd* va lancer (ce ne sera pas l'utilisateur qui le lancera à la main).  
Quand on utilise des commandes `systemctl` pour démarrer des machins, ces machins, c'est des *services*.

Notre but dans cette section : **créer un service `efrei_server.service` qui lance le programme `efrei_server`**.

> Vous allez voir que ça va nous permettre rapidement plein de choses, faciliter l'administration mais aussi améliorer le niveau de sécurité de l'application. *systemd* ne se contente pas de lancer le programme à notre place, mais peut faire des choses bien plus avancées si on lui demande.

## Sommaire

- [II. Servicer le programme](#ii-servicer-le-programme)
  - [Sommaire](#sommaire)
  - [1. Création du service](#1-création-du-service)
  - [2. Tests](#2-tests)

## 1. Création du service

🌞 **Créer un service `efrei_server.service`**

- pour cela, il faut créer le fichier suivant : `/etc/systemd/system/efrei_server.service`
- avec le contenu (simpliste) suivant :

> *Vous pouvez le nommer autrement (ou pas) parce que `efrei_server` c kan meme super niul kom nom.*

```systemd
[Unit]
Description=Super serveur EFREI
 
[Service]
ExecStart=/usr/local/bin/efrei_server
Environment=
EnvironmentFile=
```

➜ **Pour les variables d'environnement** :

- choisissez **SOIT** `Environment=` **SOIT** `EnvironmentFile=` (l'un ou l'autre)
  - `Environment=` permet de préciser les variables une par une directement
  - `EnvironmentFile=` permet de préciser le chemin vers un fichier qui contient les variables d'environnement (plus clean que de foutre le bordel dans ce fichier)

➜ **Une fois le fichier `/etc/systemd/system/efrei_server.service` créé** :

- exécutez la commande `systemctl daemon-reload`
  - cela ordonne à *systemd* de relire les fichiers de *service*
  - il va alors repérer votre nouveau `efrei_server.service`
  - c'est **strictement nécessaire** de taper cette commande à chaque fois que vous modifier un fichier `.service`

```bash
[meow@efrei-xmg4agau1 ~]$ sudo systemctl daemon-reload
[meow@efrei-xmg4agau1 ~]$ cat /etc/systemd/system/efrei_server.service
[Unit]
Description=Super serveur EFREI

[Service]
ExecStart=/usr/local/bin/efrei_server
EnvironmentFile=/var/lib/efrei_server/efrei_server.env

[meow@efrei-xmg4agau1 ~]$ cat /var/lib/efrei_server/efrei_server.env
LISTEN_ADDRESS=192.168.86.4
```
## 2. Tests

🌞 **Exécuter la commande `systemctl status efrei_server`**

- le nom qu'on tape ici : `efrei_server`, c'est le nom du fichier
- vous devriez voir que votre service est inactif
```bash
[meow@efrei-xmg4agau1 ~]$ sudo systemctl status efrei_server
○ efrei_server.service - Super serveur EFREI
     Loaded: loaded (/etc/systemd/system/efrei_server.service; static)
     Active: inactive (dead)
```

🌞 **Démarrer le service**

- avec une commande `systemctl` adaptée

➜ Vous pourrez **voir les logs du service** avec la commande `journalctl -xe -u efrei_server` (si vous avez appelé le *service* `efrei_server.service`, adaptez sinon)
```bash
[meow@efrei-xmg4agau1 ~]$ sudo systemctl start efrei_server
```

🌞 **Vérifier que le programme tourne correctement**

- avec une commande `systemctl` adaptée, afficher le statut du *service* `efrei_server`
- avec une commande `ss` adaptée, prouver que le programme écoute sur l'adresse IP souhaitée
- depuis votre PC, connectez-vous au service, en utilisant une commande `nc`
```bash
sudo systemctl status efrei_server
● efrei_server.service - Super serveur EFREI
     Loaded: loaded (/etc/systemd/system/efrei_server.service; static)
     Active: active (running) since Tue 2024-09-10 12:13:18 CEST; 1s ago
   Main PID: 5629 (efrei_server)
      Tasks: 2 (limit: 23169)
     Memory: 32.5M
        CPU: 457ms
     CGroup: /system.slice/efrei_server.service
             ├─5629 /usr/local/bin/efrei_server
             └─5630 /usr/local/bin/efrei_server

Sep 10 12:13:18 efrei-xmg4agau1.etudiants.campus.villejuif systemd[1]: Started Super serveur EFREI.

[meow@efrei-xmg4agau1 ~]$ ss -tuln | grep 8888
tcp   LISTEN 0      100     192.168.86.4:8888      0.0.0.0:*
tcp   LISTEN 0      100        127.0.0.1:8888      0.0.0.0:*

PS C:\Users\1dk1d> ncat.exe 192.168.86.4 8888
Hello ! Tu veux des infos sur quoi ?
1) cpu
2) ram
3) disk
4) ls un dossier

Ton choix (1, 2, 3 ou 4) :
```

> ➜ [**Lien vers la partie 3**](./part3.md)
