# III. MAKE SERVICES GREAT AGAIN

Bon le programme est lancé en tant que *service*, mais il est toujours bien pété et les risques sont toujours les mêmes en cas d'exploitation.

**On va prendre plusieurs mesures :**

- proposer une politique de **restart automatique**
- maîtriser **l'emplacement et les permissions** des fichiers liés à l'application
- lancer l'app en tant qu'un **utilisateur** spécifique qui n'a que peu de privilèges sur la machine
- **empêcher les connexions réseau** intempestives (il peut recevoir des clients sur un port spécifique uniquement)
- protéger l'app contre le **flood**
- **isoler** l'application du reste du système
- empêcher le service de faire **des actions indésirables**

**A la fin on aura un programme vulnérable qui :**

- ne pourra pas lancer d'autre programmes
- n'aura aucun droit de lecture/écriture à part là où il a rigoureusement besopin
- sera isolé du reste du système (il ne PEUT PAS accéder à certaines ressources du système)
- sera surveillé
- pourra accéder au réseau de façon très limitée
- sera protégé des attaques par spam trop sommaires

	➜ **Autrement dit** : on sera chills, et on pourra aller engueuler les dévs de proposer des trucs tout pourris comme ça.

## Sommaire

- [III. MAKE SERVICES GREAT AGAIN](#iii-make-services-great-again)
  - [Sommaire](#sommaire)
  - [1. Restart automatique](#1-restart-automatique)
  - [2. Utilisateur applicatif](#2-utilisateur-applicatif)
  - [3. Maîtrisez l'emplacement des fichiers](#3-maîtrisez-lemplacement-des-fichiers)
  - [4. Security hardening](#4-security-hardening)

## 1. Restart automatique

Bon pour ça, facile, on va juste faire en sorte que si le programme coupe, il soit relancé automatiquement.

🌞 **Ajoutez une clause dans le fichier `efrei_server.service` pour le restart automatique**

- c'est la clause `Restart=`
- trouvez la valeur adaptée pour qu'il redémarre tout le temps, dès qu'il est coupé

`Restart=always`

🌞 **Testez que ça fonctionne**

- lancez le *service* avec une commande `systemctl`
- affichez le processus lancé par *systemd* avec une commande `ps`
  - je veux que vous utilisiez une commande avec `| grep quelquechose` pour n'afficher que la ligne qui nous intéresse
  - vous devriez voir un processus `efrei_server` qui s'exécute
- tuez le processus manuellement avec une commande `kill`
- constatez que :
  - le service a bien été relancé
  - il y a bien un nouveau processus `efrei_server` qui s'exécute

> Pour rappel, **TOUTES** les commandes pour faire ce qui est demandé avec un 🌞 doivent figurer dans le compte-rendu.

```bash
[meow@efrei-xmg4agau1 ~]$ ps aux | grep "efrei"
root        5629  0.0  0.0   2956  1940 ?        Ss   12:13   0:00 /usr/local/bin/efrei_server
root        5630  0.0  0.6  33772 25788 ?        S    12:13   0:00 /usr/local/bin/efrei_server

[meow@efrei-xmg4agau1 ~]$ ps aux | grep "efrei"
root        5965  0.0  0.0   2956  1944 ?        Ss   13:38   0:00 /usr/local/bin/efrei_server
root        5966  0.0  0.6  33772 26176 ?        S    13:38   0:00 /usr/local/bin/efrei_server
meow        6011  0.0  0.0   6408  2300 pts/1    S+   13:47   0:00 grep --color=auto efrei
[meow@efrei-xmg4agau1 ~]$ sudo kill -9 5965
[meow@efrei-xmg4agau1 ~]$ ps aux | grep "efrei"
root        6016  9.6  0.0   2956  1944 ?        Ss   13:48   0:00 /usr/local/bin/efrei_server
root        6017  3.3  0.6  33772 25776 ?        S    13:48   0:00 /usr/local/bin/efrei_server
meow        6019  0.0  0.0   6408  2304 pts/1    S+   13:48   0:00 grep --color=auto efrei
```
## 2. Utilisateur applicatif

Lorsqu'un programme s'exécute sur une machine (peu importe l'OS ou le contexte), le programme est **toujours** exécuté sous l'identité d'un utilisateur.  
Ainsi, pendant son exécution, le programme aura les droits de cet utilisateur.  

> Par exemple, un programme lancé en tant que `toto` pourra lire un fichier `/var/log/toto.log` uniquement si l'utilisateur `toto` a les droits sur ce fichier.

🌞 **Créer un utilisateur applicatif**

- c'est lui qui lancera `efrei_server`
- avec une commande `useradd`
- choisissez...
  - un nom approprié
  - un homedir approprié
  - un shell approprié

> N'hésitez pas à venir vers moi pour discuter de ce qui est le plus "approprié" si nécessaire.

`[meow@efrei-xmg4agau1 ~]$ sudo useradd -m -d /home/efrei_user -s /usr/sbin/nologin efrei_user`

🌞 **Modifier le service pour que ce nouvel utilisateur lance le programme `efrei_server`**

- je vous laisse chercher la clause appropriée à ajouter dans le fichier `.service`

`User=efrei_user`

🌞 **Vérifier que le programme s'exécute bien sous l'identité de ce nouvel utilisateur**

- avec une commande `ps`
- encore là, filtrez la sortie avec un `| grep`
- n'oubliez pas de redémarrer le service pour que ça prenne effet hein !

> *Déjà à ce stade, le programme a des droits vraiment limités sur le système.*

```bash
[meow@efrei-xmg4agau1 ~]$ sudo cat /etc/systemd/system/efrei_server.service
[Unit]
Description=Super serveur EFREI

[Service]
ExecStart=/home/efrei_user/efrei_server
EnvironmentFile=/var/lib/efrei_server/efrei_server.env
User=efrei_user
Restart=always

[meow@efrei-xmg4agau1 ~]$ sudo systemctl status efrei_server
● efrei_server.service - Super serveur EFREI
     Loaded: loaded (/etc/systemd/system/efrei_server.service; static)
     Active: active (running) since Tue 2024-09-10 15:06:06 CEST; 3s ago
   Main PID: 6611 (efrei_server)
      Tasks: 2 (limit: 23169)
     Memory: 32.5M
        CPU: 76ms
     CGroup: /system.slice/efrei_server.service
             ├─6611 /home/efrei_user/efrei_server
             └─6612 /home/efrei_user/efrei_server

Sep 10 15:06:06 efrei-xmg4agau1.etudiants.campus.villejuif systemd[1]: Started Super serveur EFREI.

[meow@efrei-xmg4agau1 ~]$ ps aux | grep "efrei"
efrei_u+    6611  0.0  0.0   2956  1940 ?        Ss   15:06   0:00 /home/efrei_user/efrei_server
efrei_u+    6612  0.0  0.6  33772 25784 ?        S    15:06   0:00 /home/efrei_user/efrei_server
```

## 3. Maîtrisez l'emplacement des fichiers

Pour fonctionner, l'application a besoin de deux choses :

- des **variables d'environnement définies**, ou des valeurs par défaut nulles seront utilisées
- un **fichier de log** où elle peut écrire
  - par défaut elle écrit dans `/tmp` comme l'indique le warning au lancement de l'application
  - vous pouvez définir la variable `LOG_DIR` pour choisir l'emplacement du fichier de logs

🌞 **Choisir l'emplacement du fichier de logs**

- créez un dossier dédié dans `/var/log/` (le dossier standard pour stocker les logs)
- indiquez votre nouveau dossier de log à l'application avec la variable `LOG_DIR`
- l'application créera un fichier `server.log` à l'intérieur

```bash
[meow@efrei-xmg4agau1 ~]$ sudo mkdir /var/log/efrei_server
[meow@efrei-xmg4agau1 ~]$ sudo cat /home/efrei_user/efrei_server.env
LISTEN_ADDRESS=192.168.86.4
LOG_DIR=/var/log/efrei_server
```

🌞 **Maîtriser les permissions du fichier de logs**

- avec les commandes `chown` et `chmod`
- appliquez les permissions les plus restrictives possibles sur le dossier dans `var/log/`

```bash
[meow@efrei-xmg4agau1 ~]$ sudo chown efrei_user:efrei_user /var/log/efrei_server
[meow@efrei-xmg4agau1 ~]$ sudo chmod 700 /var/log/efrei_server
```

## 4. Security hardening

Il existe beaucoup de clauses qu'on peut ajouter dans un fichier `.service` pour que *systemd* s'occupe de sécuriser le service, en l'isolant du reste du système par exemple.

Ainsi, une commande est fournie `systemd-analyze security` qui permet de voir quelles mesures de sécurité on a activé. Un score (un peu arbitraire) est attribué au *service* ; cela représente son "niveau de sécurité".

Cette commande est **très** pratique d'un point de vue pédagogique : elle va vous montrer toutes les clauses qu'on peut ajouter dans un `.service` pour renforcer sa sécurité.

🌞 **Modifier le `.service` pour augmenter son niveau de sécurité**

- ajoutez au moins 5 clauses dans le fichier pour augmenter le niveau de sécurité de l'application
- n'utilisez que des clauses que vous comprenez, useless sinon

```bash
[meow@efrei-xmg4agau1 ~]$ sudo cat /etc/systemd/system/efrei_server.service
[Unit]
Description=Super serveur EFREI

[Service]
ExecStart=/home/efrei_user/efrei_server
EnvironmentFile=/home/efrei_user/efrei_server.env
User=efrei_user
Restart=always

NoNewPrivileges=yes 
PrivateTmp=yes 
PrivateDevices=yes 
DevicePolicy=closed 
ProtectSystem=strict

[meow@efrei-xmg4agau1 ~]$ sudo systemd-analyze security efrei_server.service
  NAME                                                        DESCRIPTION                                                             EXPOSURE
✓ User=/DynamicUser=                                          Service runs under a static non-root user identity
✓ NoNewPrivileges=                                            Service processes cannot acquire new privileges
✓ AmbientCapabilities=                                        Service process does not receive ambient capabilities
✓ ProtectSystem=                                              Service has strict read-only access to the OS file hierarchy
✓ SupplementaryGroups=                                        Service has no supplementary groups
✓ CapabilityBoundingSet=~CAP_SYS_RAWIO                        Service has no raw I/O access
✓ PrivateTmp=                                                 Service has no access to other software's temporary files
✓ PrivateDevices=                                             Service has no access to hardware devices
✓ DeviceAllow=                                                Service has a minimal device ACL
✓ KeyringMode=                                                Service doesn't share key material with other services
✓ Delegate=                                                   Service does not maintain its own delegated control group subtree
✓ NotifyAccess=                                               Service child processes cannot alter service state
✓ PrivateMounts=                                              Service cannot install system mounts
✓ CapabilityBoundingSet=~CAP_MKNOD                            Service cannot create device nodes
```

🌟 **BONUS : Essayez d'avoir le score le plus haut avec `systemd-analyze security`**

➜ 💡💡💡 **A ce stade, vous pouvez ré-essayez l'injection que vous avez trouvé dans la partie 1. Normalement, on peut faire déjà moins de trucs avec.**

> ➜ [**Lien vers la partie 4**](./part4.md)
