# Partie 4 : Autour de l'application

Dans cette dernière partie, on va s'intéresser toujours à améliorer le niveau de sécurité de l'application.  
Mais cette fois-ci en s'intéressant un peu plus à ce qu'on peut faire à l'extérieur du service.

## Sommaire

- [Partie 4 : Autour de l'application](#partie-4--autour-de-lapplication)
  - [Sommaire](#sommaire)
  - [1. Firewalling](#1-firewalling)
  - [2. Protéger l'app contre le flood](#2-protéger-lapp-contre-le-flood)
  - [3. Empêcher le programme de faire des actions indésirables](#3-empêcher-le-programme-de-faire-des-actions-indésirables)

## 1. Firewalling

**Le *firewall* permet de filtrer les connexions entrantes sur la machine, mais aussi les connexions sortantes.**

Une fois que notre serveur est en place, et qu'il héberge notre super service, il n'y à priori que très peu de choses qu'on veut autoriser

- notre service doit accueillir les clients sur un port spécifique
- notre service SSH doit rester accessible sur le port 22

Ca sous-entend que toutes autres accès réseau doit être bloqué, par exemple :

- des connexions entrantes sur d'autres ports
- n'importe quelle connexion sortante

> **Oui on bloque tout en sortie !** C'est une mesure de sécurité simple et très forte. Seul inconvénient : il faudra désactiver temporairement cette règle pour mettre à jour le serveur quand c'est nécessaire (sinon il ne peut pas utiliser le réseau, pour télécharger des paquets par exemple).

🌞 **Configurer de façon robuste le firewall**

- bloquer toutes les connexions sortantes
- bloquer toutes les connexions entrantes (y compris le ping) à part si c'est à destination du serveur SSH ou du service `efrei_admin`

```bash
[meow@efrei-xmg4agau1 root]$ sudo firewall-cmd --state
running
[meow@efrei-xmg4agau1 root]$ sudo firewall-cmd --permanent --add-service=ssh
Warning: ALREADY_ENABLED: ssh
success
[meow@efrei-xmg4agau1 ~]$ sudo firewall-cmd --permanent --add-port=8888/tcp
[sudo] password for meow:
Warning: ALREADY_ENABLED: 8888:tcp
success
[meow@efrei-xmg4agau1 ~]$ sudo firewall-cmd --set-default-zone=drop
success
[meow@efrei-xmg4agau1 ~]$ sudo firewall-cmd --permanent --zone=drop --add-service=ssh
success
[meow@efrei-xmg4agau1 ~]$ sudo firewall-cmd --permanent --zone=drop --add-port=8888/tcp
success
[meow@efrei-xmg4agau1 ~]$ sudo firewall-cmd --reload
success
```

🌞 **Prouver que la configuration est effective**

- prouver que les connexions sortantes sont bloquées
- prouver que les pings sont bloqués, mais une connexion SSH fonctionne

```bash
[meow@efrei-xmg4agau1 ~]$ sudo firewall-cmd --list-all
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 8888/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

PS C:\Users\1dk1d> ping 192.168.86.4

Envoi d’une requête 'Ping'  192.168.86.4 avec 32 octets de données :
Ctrl+C
PS C:\Users\1dk1d> ssh meow@192.168.86.4
meow@192.168.86.4's password:
Last login: Tue Sep 10 17:08:07 2024
[meow@efrei-xmg4agau1 ~]$ exit
logout
Connection to 192.168.86.4 closed.
```

## 2. Protéger l'app contre le flood

Fail2ban notre vieil ami ! Fail2ban est un outil classique sur les OS GNU/Linux.

**Le fonctionnement de fail2ban est simpliste :**

- on lui demande surveiller un fichier donné
- on définit un pattern à repérer dans ce fichier
- si plusieurs lignes correspondant au pattern se répètent, il effectue une action
- par exemple, on ajoute une règle firewall

> Quand on configure fail2ban pour surveiller un certain fichier, on dit qu'on crée une *jail* fail2ban.

**Cas concret ici :**

- dès qu'un client se connecte à notre service, une ligne de log est ajouté au fichier de log
- cette ligne de log contient l'IP du client qui s'est connecté
- si un client se connecte + de 5 fois en moins de 10 secondes (par exemple) on peut estimer que c'est du flood (tentative de DOS ?)
- il faudrait blacklister automatiquement l'IP de ce client dans le firewall
- fail2ban fait exactement ça

🌞 **Installer fail2ban sur la machine**

```bash
sudo dnf install epel-release 
sudo dnf install fail2ban
```

🌞 **Ajouter une *jail* fail2ban**

- elle doit lire le fichier de log du service, que vous avez normalement placé dans `/var/log/`
- repérer la ligne de connexion d'un client
- blacklist à l'aide du firewall l'IP de ce client

```bash
[meow@efrei-xmg4agau1 ~]$ sudo cat /etc/fail2ban/jail.local
[efrei_server]
enabled = true
filter = efrei_server
logpath = /var/log/efrei_server/server.log
maxretry = 5
bantime = 600
findtime = 10
action = firewallcmd-ipset

[meow@efrei-xmg4agau1 ~]$ sudo cat /etc/fail2ban/filter.d/efrei_server.conf
[Definition]
failregex = .*Received.*from \('<HOST>', [0-9]+\).*
```

🌞 **Vérifier que ça fonctionne !**

- faites-vous ban ! En faisant plein de connexions rapprochées avec le client
- constatez que le ban est effectif
- levez le ban (il y a une commande pour lever un ban qu'a réalisé fail2ban)

```bash
[meow@efrei-xmg4agau1 ~]$ python3 crash.py
Connexion 1 réussie et fermée.
Connexion 2 réussie et fermée.
Connexion 3 réussie et fermée.
Connexion 4 réussie et fermée.
Connexion 5 réussie et fermée.
Connexion 6 réussie et fermée.
Connexion 7 réussie et fermée.
Connexion 8 réussie et fermée.
Erreur lors de la connexion 9: [Errno 111] Connection refused
Erreur lors de la connexion 10: [Errno 111] Connection refused
Toutes les connexions ont été réalisées et fermées.

[meow@efrei-xmg4agau1 ~]$ sudo fail2ban-client status efrei_server
Status for the jail: efrei_server
|- Filter
|  |- Currently failed: 1
|  |- Total failed:     15
|  `- File list:        /var/log/efrei_server/server.log
`- Actions
   |- Currently banned: 1
   |- Total banned:     1
   `- Banned IP list:   192.168.86.4

[meow@efrei-xmg4agau1 ~]$ sudo fail2ban-client unban 192.168.86.4
1
[meow@efrei-xmg4agau1 ~]$ sudo fail2ban-client status efrei_server
Status for the jail: efrei_server
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     15
|  `- File list:        /var/log/efrei_server/server.log
`- Actions
   |- Currently banned: 0
   |- Total banned:     1
   `- Banned IP list:
```
## 3. Empêcher le programme de faire des actions indésirables

Lors de son fonctionnement, un programme peut être amené à exécuter des **appels système** (ou *syscalls*) en anglais.  
Un programme **doit** exécuter un *syscall* dès qu'il veut interagir avec une ressource du système. Par exemple :

- lire/modifier un fichier
- établir une connexion réseau
- écouter sur un port
- changer les droits d'un fichier
- obtenir la liste des processus
- lancer un nouveau processus
- etc.

➜ **Exécuter un *syscall* c'est demander au kernel de faire quelque chose.**

Ainsi, par exemple, quand on exécute la commande `cat` sur un fichier pour lire son contenu, **la commande `cat` va exécuter (entre autres) le *syscall* `open` afin de pouvoir ouvrir et lire le fichier**.

> Il se passe la même chose quand genre t'utilises Discord, et t'envoies un fichier à un pote. L'application Discord va exécuter un *syscall* pour obtenir le contenu du fichier, et l'envoyer sur le réseau.

Si le programme est exécuté par **un utilisateur qui a les droits sur ce fichier, alors le kernel autorisera ce *syscall*** et le programme `cat` pourra accéder au contenu du fichier sans erreur, et l'afficher dans le terminal.

> Dit autrement : n'importe quel programme qui accède au contenu d'un fichie (par exemple) exécute **forcément** un *syscall* pour obtenir le contenu de ce fichier. Peu importe l'OS, c'est un truc commun à tous.

➜ ***seccomp* est un outil qui permet de filtrer les *syscalls* qu'a le droit d'exécuter un programme**

On définit une liste des *syscalls* que le programme a le droit de faire, les autres seront bloqués.

> Par exemple, un *syscall* sensible est `fork()` qui permet de créer un nouveau processus.

Dans notre cas, avec notre ptit *service*, c'est un des problèmes :

- vous injectez du code dans l'application en tant que vilain hacker
- pour exécuter des programmes comme `cat` ou autres
- à chaque commande exécutée avec l'injection, un *syscall* est exécuté par le programme serveur pour demander la création d'un nouveau processus (votre injection)
- on pourrait bloquer totalement ce comportement : empêcher le *service* de lancer un autre processus que `efrei_server`

🌞 **Ajouter une politique seccomp au fichier `.service`**

- la politique doit être la plus restrictive possible
- c'est à dire que juste le strict minimum des *syscalls* nécessaires doit être autorisé

```bash
[meow@efrei-xmg4agau1 ~]$ sudo cat /etc/systemd/system/efrei_server.service
[Unit]
Description=Super serveur EFREI

[Service]
ExecStart=/home/efrei_user/efrei_server
EnvironmentFile=/home/efrei_user/efrei_server.env
User=efrei_user
Restart=always

SystemCallFilter=write openat newfstatat rt_sigaction close mprotect read pread64 futex brk dup bind socket listen setsockopt gettid getpid epoll_wait

NoNewPrivileges=yes
PrivateTmp=yes
PrivateDevices=yes
DevicePolicy=closed
ProtectSystem=strict
```