# TP1 : Héberger un service
Dans ce TP de remise à niveau, j'ai choisi de vous faire **héberger un service**.  
Ca va nous permettre de toucher à plein d'aspects des systèmes GNU/Linux, et j'ai **très largement saupoudré le tout avec de la sécu**.
On va sortir un peu des cas d'école avec Apache, NGINX étou.  
**Enfet enfet, j'ai développé une app Python nulle, et va falloir l'héberger. Et ptet la péter.**
Vous allez juste avoir un programme qui fait des machins ~~pas très utiles~~. A vous de le faire tourner proprement comme un service sûr. Bien sûr je vous guide raisonnablement tout le long du TP pour faire ça !
> *Il est ptet un peu pété en terme de sécu mon programme... hihi.*
# Sommaire
- [TP1 : Héberger un service](#tp1--héberger-un-service)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
- [Suite du TP](#suite-du-tp)
# 0. Prérequis
➜ **Rendu avec un dépôt Git**
- rendu format Markdown donc
- créez-le tout de suite votre dépôt
- et envoyez-moi un lien par MP Discord avec votre nom de famille et l'URL vers votre dépôt
➜ **Dès que vous voyez l'emoji 🌞 c'est qu'il y a quelque chose à faire qui doit figurer dans le rendu**, des commandes à taper par ex
➜ **Une VM avec un OS Linux de votre choix**
- je vous recommande une base RedHat comme Rocky Linux, comme d'hab
- mais vous êtes libres, si vous souhaitez travailler avec un autre OS
- pas de GUI pour votre VM évidemment !
➜ **La VM doit avoir...**
- une interface réseau pour **accéder à internet** (NAT avec VirtualBox)
- une interface réseau pour **accéder à un réseau local avec le PC hôte** (host-only dans VirtualBox)
- un **accès SSH fonctionnel**, pour que vous puissiez l'administrer
- un **firewall activé**, qui bloque toutes les connexions entrantes par défaut (à part SSH)
🌞 **Boom ça commence direct : je veux l'état initial du firewall**
- dans le compte-rendu, une commande qui me montre l'état initial de votre firewall (qui ne doit autoriser que le trafic SSH en entrée)
```bash
firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
➜ **Gestion d'utilisateurs**
- la machine doit posséder un user qui porte votre prénom ou pseudo ou truc du genre
- il est interdit d'utiliser `root` directement
- si besoin des privilèges de `root`, vous utiliserez exclusivement la commande `sudo`
🌞 **Fichiers /etc/sudoers /etc/passwd /etc/group** dans le dépôt de compte-rendu svp !
```
cat /etc/sudoers
## The COMMANDS section may have other options added to it.
##
## Allow root to run any commands anywhere
root    ALL=(ALL)     ALL
meow    ALL=(ALL)       ALL
## Allows members of the 'sys' group to run networking, software,
## service management apps and more.
# %sys ALL = NETWORKING, SOFTWARE, SERVICES, STORAGE, DELEGATING, PROCESSES, LOCATE, DRIVERS

## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)     ALL

## Same thing without a password
# %wheel     ALL=(ALL)  NOPASSWD: ALL

## Allows members of the users group to mount and unmount the
## cdrom as root
# %users  ALL=/sbin/mount /mnt/cdrom, /sbin/umount /mnt/cdrom

## Allows members of the users group to shutdown this system
# %users  localhost=/sbin/shutdown -h now

## Read drop-in files from /etc/sudoers.d (the # here does not mean a comment)
#includedir /etc/sudoers.d

cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin
systemd-coredump:x:999:997:systemd Core Dumper:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
tss:x:59:59:Account used for TPM access:/dev/null:/sbin/nologin
sssd:x:998:995:User for sssd:/:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/usr/share/empty.sshd:/sbin/nologin
chrony:x:997:994:chrony system user:/var/lib/chrony:/sbin/nologin
systemd-oom:x:992:992:systemd Userspace OOM Killer:/:/usr/sbin/nologin
tcpdump:x:72:72::/:/sbin/nologin
meow:x:1000:1000::/home/meow:/bin/bash

cat /etc/group
root:x:0:
bin:x:1:
daemon:x:2:
sys:x:3:
adm:x:4:
tty:x:5:
disk:x:6:
lp:x:7:
mem:x:8:
kmem:x:9:
wheel:x:10:meow
cdrom:x:11:
mail:x:12:
man:x:15:
dialout:x:18:
floppy:x:19:
games:x:20:
tape:x:33:
video:x:39:
ftp:x:50:
lock:x:54:
audio:x:63:
users:x:100:
nobody:x:65534:
utmp:x:22:
utempter:x:35:
input:x:999:
kvm:x:36:
render:x:998:
systemd-journal:x:190:
systemd-coredump:x:997:
dbus:x:81:
ssh_keys:x:996:
tss:x:59:
sssd:x:995:
sshd:x:74:
chrony:x:994:
sgx:x:993:
systemd-oom:x:992:
tcpdump:x:72:
meow:x:1000:
```
# Suite du TP
Vous pouvez enchaîner sur les 3 parties du TP, dans l'ordre :
- [**Partie 1** : Host & Hack](./part1.md)
- [**Partie 2** : Servicer le programme](./part2.md)
- [**Partie 3** : MAKE SERVICES GREAT AGAIN](./part3.md)
- [**Partie 4** : Autour de l'application](./part4.md)
> *Ui le verbe "servicer" existe po. J'dois écrire que des trucs qui ont du sens maintenant ?*